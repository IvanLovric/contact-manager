var app = angular.module("ContactManagerApp", ['ngRoute'])

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
        .when('/', {
            title: 'Home',
            templateUrl: './views/Home.html',
            controller: 'ContactController'
        })
        .otherwise({
            redirectTo: '/'
        });
    }]);
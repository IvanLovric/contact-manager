app.controller('ContactController', function ($scope, ContactService) {

	Refresh();
	//Get all contact records  
	function Refresh() {
		var getContactsData = ContactService.getAllContacts();
		getContactsData.then(function(response){
			//Successful
			console.log("Data received from server...");
			$scope.contacts = response.data;;
		}, function(){
			//Error
			alert('Error in getting contact records');
		});
	$scope.contact = "";
	};

	//Add new contact record
	$scope.addNewContact = function(contact){
		var _contact = contact;
		ContactService.addContact(_contact).then(function(response){
			//Successful
			console.log("Data sent to server...");
		}, function(){
			//Error
			alert('Error in adding new contact record');
		});
		Refresh();
	};

	//Delete contact record
	$scope.removeContact = function(id){
		var _id = id;
		ContactService.deleteContact(_id).then(function(response){
			//Successful
			console.log(_id + " is deleted...");
			Refresh();
		}, function(){
			//Error
			alert('Error in deleting contact record');
		});
	};

	//Edit contact record
	$scope.editContact = function(id){
		var _id = id;
		ContactService.getSingleContact(_id).then(function(response){
			//Successful
			console.log("Editing " +_id +" ...");
			$scope.contact = response.data; //put the response date into contact ng-model 
		}, function(){
			//Error
			alert('Error in editing contact record');
		});
	};
	
	//Update contact record
	$scope.updateContact = function(contact){
		var _id = contact._id;
		var _contact = contact;
		ContactService.changeContact(_id, _contact).then(function(response){
			//Successful
			console.log("Updating " +_id +" ...");
			Refresh(); 
		}, function(){
			//Error
			alert('Error in updating contact record');
		});
	};

});
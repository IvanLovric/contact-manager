app.service("ContactService", function ($http) {

    //GET all contacts from server
    this.getAllContacts = function () {
        return $http.get('/contacts');
    };

    //POST contact to server
    this.addContact = function (contact) {
        return $http.post('/contacts',contact);
    };

    //DELETE contact 
    this.deleteContact = function (id) {
        return $http.delete('/contacts/' + id);
    };

    //GET single contact from server
    this.getSingleContact = function (id) {
        return $http.get('/contacts/' + id);
    };

    //PUT contact changes to server
    this.changeContact = function (id, contact) {
        return $http.put('/contacts/' + id, contact);
    };

});
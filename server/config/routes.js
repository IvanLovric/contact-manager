var express = require('express');
var mongojs = require('mongojs');
var bodyParser = require('body-parser');
var db = mongojs('ContactManagerDB', ['ContactManagerDB']); //koju bazu i koju kolekciju koristimo


module.exports = function(app){

	app.use(bodyParser.json()); //server can parse body from inputs he recieves

	app.get('/contacts', function (req, res) {
		console.log("I recieved a GET request...");
		db.ContactManagerDB.find(function (err, docs){
			console.log(docs);
			res.json(docs);
		});
	});

	app.post('/contacts', function(req, res){
		console.log(req.body);
		db.ContactManagerDB.insert(req.body, function(err, doc){
			res.json(doc);
		});
	});

	app.delete('/contacts/:id', function(req, res){
		var id = req.params.id;
		console.log(id + " is being deleted...");
		db.ContactManagerDB.remove({_id: mongojs.ObjectId(id)}, function(err, doc){
			res.json(doc);
		});
	});

	app.get('/contacts/:id', function(req, res){
		var id = req.params.id;
		console.log("Editing " + id + " ...");
		db.ContactManagerDB.findOne({_id: mongojs.ObjectId(id)}, function(err, doc){
			res.json(doc);
		});
	});

	app.put('/contacts/:id', function(req, res){
		var id = req.params.id;
		console.log(req.body);
		db.ContactManagerDB.findAndModify({query: {_id: mongojs.ObjectId(id)},
			update: {$set: {name: req.body.name, email: req.body.email, contact: req.body.contact}},
			new: true }, function(err, doc){
				res.json(doc);
			});
	});
}
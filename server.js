var express = require('express');
var app = express();
var config = require('./server/config/routes')(app);

//render data from /public folder 
app.use(express.static(__dirname + "/public"));

// startup our app at http://localhost:3030
var port = process.env.PORT || 3030;
app.listen(port);   
console.log('Server running on port ' + port + '...');
